"""A script to print compilation times per file sorted from greater to smaller.
   You need to build using the command line option /Bt+ in VS, then paste output into a file,
   then provide that file as argument to the script"""

import sys
import re

def main():
	filename = sys.argv[1]
	f = open(filename, 'r')
	
	times = list()
	for l in f.readlines():
		time = re.findall('([\.0-9]+)s', l)
		if len(time) > 0:
			filename = re.findall('BB (.*)', l)
			if len(filename) > 0:
				times.append( (float(time[0]), filename[0]))
				
	times.sort(reverse=True)
	for t,filename in times:
		print filename, ', ', t
	
main()