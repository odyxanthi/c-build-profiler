""" A script for extracting inclusion counts per file from build output.
    First enable the /showIncludes option in Visual Studio to see the included files per compilation unit.
    Store the build output to a file, then provide the file as input to the script.
"""

import sys
import re
import shutil
import os
from extractTopLevelHeaders import *

#g_matchPattern = '.*Note: including file:\s*(.*)'                    #matches all included files
#g_matchPattern = '.*Note: including file:\s*(.*boost.*)'             #matches only boost files
#g_matchPattern = '.*Note: including file:\s*(.*geometrix.*)'         #matches only geometrix files
#g_matchPattern = '.*Note: including file:\s*(.*LegionUtility.*)'      #matches only LegionUtility files
g_matchPattern = '.*Note: including file:\s*(.*GenericObject.*)'      #matches only GenericObject files
#g_matchPattern = '.*Note: including file:\s*(.*GIT_Legion_mine.*)'

g_exclusionList = [
  '.*boost.*detail.*'
, '.*boost.*config.*'
#, '.*boost.*'
#, '.*GIT_Legion_mine.*'  
#, '.*LegionUtility.*'  
, '.*LegionSimulator.*'
, '.*cadgeometry.*'  
, '.*ObjectDatabase.*'
, '.*GIT_Legion_mine.Geo.*'  
#, '.*ObjectDatabase.*'
]


def msvc13_IncludeMsgPattern():
    return '.*Note: including file:\s*'


def getFirstMatchingPattern(text, patterns):
    for p in patterns:
        matches = re.findall(p, text, re.IGNORECASE)
        if len(matches) > 0:
            return matches[0]
    return None


def calculateInclusionCounts(buildOutputLines, inclusionPatterns, exclusionPatterns, leafPatterns=[], includeMsgPattern = msvc13_IncludeMsgPattern()):

    buildOutputLines = hideChildHeaders(buildOutputLines, leafPatterns)
    inclusionsPerFile = dict()
    originalFileNames = dict()
    inclusionPatterns_ = map(lambda s: includeMsgPattern+'('+ s +')', inclusionPatterns )
    print 'original match pat: ', inclusionPatterns
    print 'match pattern: ' ,inclusionPatterns_
    for line in buildOutputLines:
        match = getFirstMatchingPattern(line, inclusionPatterns_)
        if match:
            filename = match
            exclusionListMatch = getFirstMatchingPattern(line, exclusionPatterns)
            if not exclusionListMatch:
                normalizedName = filename.lower().replace('\\','/')
                inclusionCounter = inclusionsPerFile.get(normalizedName, 0)   # convert to lower case and use as key
                inclusionsPerFile[normalizedName] = inclusionCounter + 1
                originalFileNames[normalizedName] = filename                  # keep the original string 
    
    l = inclusionsPerFile.items()
    l.sort(key = lambda t: t[1], reverse = True)

    inclusionsPerFile_originalFilename = list()
    for file, count in l:
        inclusionsPerFile_originalFilename.append( (originalFileNames[file], count ))
    return inclusionsPerFile_originalFilename


def main():
    if len(sys.argv) < 2:
        print 'usage: extractMostIncludedHeaders.py <file>'
        return

    filename = sys.argv[1]
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

    inclusionCounts = calculateInclusionCounts(lines, g_matchPattern, g_exclusionList)

    print 'file', ', ', 'times included'
    for file, count in inclusionCounts:
        print file, ', ', count

if __name__ == '__main__':
	main()