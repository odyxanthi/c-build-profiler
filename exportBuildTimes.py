import sys
import re
import shutil
import os
import subprocess
import datetime as dt


class ProjectInfo():
    def __init__(self):
        self.projectName = ''
        self.startTime = None
        self.durationInSeconds = None


def getProjectInfoById(projInfoDict, id):
    if id in projInfoDict:
        return projInfoDict[id]
    else:
        projInfoDict[id] = ProjectInfo()
        return projInfoDict[id]


def stringToTime(s):
    items = s.split(':')
    return dt.datetime.combine(dt.datetime.today(), dt.time(int(items[0]), int(items[1]), int(items[2])))


def optionalToStr(item):
    if item is None:
        return '?'
    else:
        return str(item)


def extractProjectNameAndID(text):
    matches = re.findall('([0-9]*)>.*Build started: Project: (.*),', text)
    if len(matches) > 0 and len(matches[0]) > 1:
        id = matches[0][0]
        projectName = matches[0][1]
        return (id, projectName)
    return None


def extractStartTimeAndID(text):
    matches = re.findall('([0-9]*)>Build started.*( \d{2}:\d{2}:\d{2})', text)
    if len(matches) > 0 and len(matches[0]) > 1:
        id = matches[0][0]
        startTime = matches[0][1]
        return (id, stringToTime(startTime))
    return None


def extractBuildDurationAndID(text):
    matches = re.findall('([0-9]+)>\s*([0-9]+) ms.*vcxproj\s+1 calls', text)
    if len(matches) > 0 and len(matches[0]) > 1:
        id = matches[0][0]
        ellapsedTime = matches[0][1]
        return (id, int(ellapsedTime))
    return None


def extractBuildTimes(lines):
    projectInfoDict = dict()
    for line in lines:
        nameAndID = extractProjectNameAndID(line)
        if nameAndID: 
            id, projectName = nameAndID
            info = getProjectInfoById(projectInfoDict, id)
            info.projectName = projectName

        startTimeAndID = extractStartTimeAndID(line)
        if startTimeAndID:
            id, startTime = startTimeAndID
            info = getProjectInfoById(projectInfoDict, id)
            info.startTime = startTime

        durationAndID = extractBuildDurationAndID(line)
        if durationAndID:
            id, duration = durationAndID
            info = getProjectInfoById(projectInfoDict, id)
            info.durationInSeconds = duration / 1000.0

    # sort based on the project start time, leaving None values at the end.
    projectInfoSorted = projectInfoDict.values()
    projectInfoSorted.sort(key=lambda info: (info.startTime is None, info.startTime))  # create a tuple e.g. (True, 23:34:55)
    overallStartTime = projectInfoSorted[0].startTime

    output_csv = list()
    output_csv.append('Overall start time(hh:mm:ss): ' + optionalToStr(overallStartTime))
    output_csv.append('Project, startTime(hh:mm:ss), startTime(sec), duration(sec), endTime(sec)')
    for info in projectInfoSorted:
        startTimeInSeconds, endTimeInSeconds = None, None
        if overallStartTime is not None:
            if info.startTime is not None:
                startTimeInSeconds = (info.startTime - overallStartTime).seconds
                if (info.durationInSeconds is not None):
                    endTimeInSeconds = startTimeInSeconds + info.durationInSeconds

        s = str(info.projectName)
        s += (', ' + optionalToStr(info.startTime))
        s += (', ' + optionalToStr(startTimeInSeconds))
        s += (', ' + optionalToStr(info.durationInSeconds))
        s += (', ' + optionalToStr(endTimeInSeconds))
        output_csv.append(s)
        
    return output_csv


def main():
    if len(sys.argv) < 2:
        print 'usage: exportBuildTimes.py <file>'
        return

    filename = sys.argv[1]
    f = open(filename, 'r')
    lines = f.readlines()

    outputLines = extractBuildTimes(lines)
    for line in outputLines:
        print line


if __name__ == '__main__':
    main()