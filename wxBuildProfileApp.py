import wx
import wx.grid
import exportBuildTimes as ebt
from extractMostIncludedHeaders import *


g_appName = "MSVC Build Profiler"

g_showOnlyFiles = [
      '.*smart_ptr.*'
    , '.*type_traits.*'
]

g_hideFiles = [
      '.*boost.*detail.*'
    , '.*boost.*config.*'
]

g_leafFiles = [
      'mpla1'
    , 'mpla2'
]


class AnalysisType:
    BUILD_TIMING = 0
    MOST_INCLUDED_HEADERS = 1


class BuildOutputPanel (wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, style=wx.TAB_TRAVERSAL)

        self._analysistType_BuildTiming      = 'Project build times'
        self._analysistType_IncludedHeaders  = 'Most included headers'

        self.buildOutputLbl = wx.StaticText(self, label="Build output")
        self.buildOutputLbl.SetMaxSize((200, 25))  

        self.buildOutputCtrl = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.HSCROLL)
        self.buildOutputCtrl.SetMaxLength(0)    # remove limit on number of chars
        
        self.processBtn = wx.Button(self, -1, 'Next (%s)' %(self._analysistType_BuildTiming) )
        self.processBtn.Bind(wx.EVT_BUTTON, parent.onBtnNext)
        self.processBtn.SetMinSize((300, 25)) 
        self.processBtn.SetMaxSize((300, 25))
        
        self.analysisTypeLbl = wx.StaticText(self, label='Analysis type:')
        self.analysisTypeLbl.SetMaxSize((200, 25))
        self.analysisTypeCombo = wx.Choice(self, 
                                             choices = [self._analysistType_BuildTiming,
                                                        self._analysistType_IncludedHeaders])
        self.analysisTypeCombo.SetMaxSize((200, 25))
        self.analysisTypeCombo.SetSelection(0)
        self.analysisTypeCombo.Bind(wx.EVT_CHOICE, self.onAnalysisTypeChanged)
        sizer1 = wx.BoxSizer(wx.HORIZONTAL)
        sizer1.Add(self.analysisTypeLbl, 0, wx.ALL, 5)
        sizer1.Add(self.analysisTypeCombo, 1, wx.TOP | wx.LEFT | wx.EXPAND, 5)
        sizer1.Add(self.processBtn, 0, wx.ALL, 5)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.buildOutputLbl, 0, wx.TOP | wx.LEFT, 5)
        sizer.Add(self.buildOutputCtrl, 1, wx.ALL | wx.EXPAND, 5)
        sizer.Add(sizer1, 0, wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()

    def __del__(self):
        pass

    def onAnalysisTypeChanged(self, e):
        index = self.analysisTypeCombo.GetSelection()
        btnTxt = 'Next (%s)' % (self.analysisTypeCombo.GetString(index))
        self.processBtn.SetLabel(btnTxt)


    def calculateBuildTimes(self, e):
        lines = self.buildOutputCtrl.GetValue().splitlines()
        results = ebt.extractBuildTimes(lines)
        self.grid.AppendText('\n'.join(results))
        
    def getBuildOutput(self):
        return self.buildOutputCtrl.GetValue().splitlines()

    def getAnalysisType(self):
        index = self.analysisTypeCombo.GetSelection()
        analysisTypeStr = self.analysisTypeCombo.GetString(index)
        if analysisTypeStr == self._analysistType_BuildTiming:
            return AnalysisType.BUILD_TIMING
        elif analysisTypeStr == self._analysistType_IncludedHeaders:
            return AnalysisType.MOST_INCLUDED_HEADERS
        else:
            raise Exception('analysis type: ' + analysisTypeStr + 'does not match existing options')

        
class BuildProfilePanel (wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(691, 565), style=wx.TAB_TRAVERSAL)

        self.grid = wx.grid.Grid(self)
        self.grid.CreateGrid(100, 5)
        self.goBackBtn = wx.Button(self, -1, "Go back (edit build output)")
        self.goBackBtn.Bind(wx.EVT_BUTTON, parent.goBack)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.grid, 1, wx.ALL | wx.EXPAND, 5)
        sizer.Add(self.goBackBtn, 0, wx.ALL, 5)

        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()


    def __del__(self):
        pass

    def setBuildTimes(self, results):
        self.grid.ClearGrid()
        self.grid.DeleteRows(0, self.grid.GetNumberRows())
        self.grid.AppendRows(len(results))
        #self.grid.CreateGrid(len(results), 7)
        
        for row, rowText in zip(range(len(results)), results):
            items = rowText.split(',')
            for col, cellVal in zip(range(len(items)), items):
                self.grid.SetCellValue(row, col, cellVal)
        

class FilterPanel(wx.Panel):
    def __init__(self, parent, filterDescription, initFilters = []):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY)
        self.descriptionLbl = wx.StaticText(self, label=filterDescription)
        self.descriptionLbl.SetMaxSize((300, 25))
        self.fitlerTxt = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.HSCROLL)
        self.fitlerTxt.SetMaxLength(0)    # remove limit on number of chars
        for txt in initFilters:
            self.fitlerTxt.AppendText(txt+'\n')

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.descriptionLbl, 0, wx.ALL, 0)
        sizer.Add(self.fitlerTxt, 1, wx.ALL | wx.EXPAND, 5)
        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()

    def getFilters(self):
        filtersTxt = self.fitlerTxt.GetValue()
        filtersTxt = filtersTxt.rstrip().lstrip()
        return filtersTxt.splitlines()


class InclusionCountAnalysisPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, id=wx.ID_ANY, pos=wx.DefaultPosition, size=wx.Size(691, 565), style=wx.TAB_TRAVERSAL)
        
        nb = wx.Notebook(self)
        self.leafFilesFilter     = FilterPanel(nb, "Don't count files included from files that match the pattern:", g_leafFiles)
        self.matchingFilesFilter = FilterPanel(nb, "Only show in the results files that match the patterns:", g_showOnlyFiles)
        self.hideFilesFilter     = FilterPanel(nb, "Hide from the results files that match the patterns:", g_hideFiles)
        nb.AddPage(self.leafFilesFilter,     "Leaf files")
        nb.AddPage(self.matchingFilesFilter, "Matching files")
        nb.AddPage(self.hideFilesFilter,     "Hidden files")
      
        self.goBackBtn = wx.Button(self, -1, "Go back (edit build output)")
        self.goBackBtn.Bind(wx.EVT_BUTTON, parent.goBack)

        self.outputGrid = wx.grid.Grid(self)
        self.outputGrid.CreateGrid(100,2)
        
        self.processBtn = wx.Button(self, -1, "Find most included headers")
        self.processBtn.Bind(wx.EVT_BUTTON, self.showInclusionCounts)
        self.processBtn.SetMaxSize((200, 25))
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        #sizer.Add(filterPanelSizer, 1, wx.ALL | wx.EXPAND, 5)
        sizer.Add(nb, 2, wx.ALL | wx.EXPAND, 5)
        sizer.Add(self.processBtn, 0, wx.ALL, 5)
        sizer.Add(self.outputGrid, 3, wx.ALL | wx.EXPAND, 5)
        sizer.Add(self.goBackBtn, 0, wx.ALL, 5)
        
        self.SetSizer(sizer)
        sizer.Fit(self)
        self.Layout()
    
    def setBuildOutput(self, buildOutputLines):
        self.buildOutputLines = buildOutputLines

    def filesToHide(self):
        return self.hideFilesFilter.getFilters()

    def filesToShow(self):
        return self.matchingFilesFilter.getFilters()

    def leafFiles(self):
        return self.leafFilesFilter.getFilters()

    def showInclusionCounts(self, e):
        hideFiles = self.filesToHide()
        showFiles = self.filesToShow()
        if len(showFiles) == 0: # default behaviour if left empty: match all files
            showFiles.append('.*')
        leafFiles = self.leafFiles()
        rows = 1
        try:
            inclusionCounts = calculateInclusionCounts(self.buildOutputLines, showFiles, hideFiles, leafFiles)
            
            self.outputGrid.ClearGrid()
            prevRows = self.outputGrid.GetNumberRows()
            if prevRows > 0:
                self.outputGrid.DeleteRows(0, prevRows)
            rows = len(inclusionCounts)
            if (rows == 0):
                msg = 'Analysis didn\'t find any header files. This can happen if build output is not of the expected form. '
                msg += 'Make sure you enable the /showIncludes option when you build your project.'
                dlg = wx.MessageDialog(self, msg, g_appName, wx.OK | wx.ICON_WARNING)
                dlg.ShowModal()
                dlg.destroy()
            else:
                self.outputGrid.AppendRows(rows)
                for row,(file,count) in zip(range(rows), inclusionCounts):
                    #print file, ', ', count
                    self.outputGrid.SetCellValue(row, 0, file)
                    self.outputGrid.SetCellValue(row, 1, str(count))
        except Exception as e:
            msg = 'Failed to extract most included headers. This can happen if provided input is not of the expected form.'
            msg += '\nDetails: ' + str(e)
            dlg = wx.MessageDialog(self, msg, g_appName, wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Destroy()

        
class BuildProfileAppFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="", pos=wx.DefaultPosition, size=wx.DefaultSize, style=wx.DEFAULT_FRAME_STYLE, name="MyFrame"):
        super(BuildProfileAppFrame, self).__init__(parent, id, title, pos, size, style, name)
        
        self.buildOutputPanel = BuildOutputPanel(self)
        self.buildOutputPanel.Show(True)
        self.buildProfilePanel = BuildProfilePanel(self)
        self.buildProfilePanel.Show(False)
        self.inclusionCountPanel = InclusionCountAnalysisPanel(self)
        self.inclusionCountPanel.Show(False)

        self.panels = [
              self.buildOutputPanel
            , self.buildProfilePanel
            , self.inclusionCountPanel
        ]
        
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.buildOutputPanel, 1, wx.EXPAND)
        self.sizer.Add(self.buildProfilePanel, 1, wx.EXPAND)
        self.sizer.Add(self.inclusionCountPanel, 1, wx.EXPAND)
        
        self.SetAutoLayout(True)
        self.SetSizer(self.sizer)
        self.Layout()

    def onBtnNext(self, e):
        print 'analysis type = ', self.buildOutputPanel.getAnalysisType()
        if self.buildOutputPanel.getAnalysisType() == AnalysisType.BUILD_TIMING:
            self.showBuildTimingPanel()
        else:
            self.showInclusionCountPanel()

    def showBuildTimingPanel(self):
        results = []
        try:
            lines = self.buildOutputPanel.getBuildOutput()
            results = ebt.extractBuildTimes(lines)
            self.buildProfilePanel.setBuildTimes(results)
            for panel in self.panels:
                panel.Hide()
            self.buildProfilePanel.Show(True)
            self.Layout()
        except Exception as e:
            msg = 'Failed to extract project build times. This can happen if provided input is not of the expected form.'
            msg += '\nDetails: ' + str(e)
            dlg = wx.MessageDialog(self, msg, g_appName, wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Destroy()


    def showInclusionCountPanel(self):
        lines = self.buildOutputPanel.getBuildOutput()
        self.inclusionCountPanel.setBuildOutput(lines)
        for panel in self.panels:
            panel.Hide()
        self.inclusionCountPanel.Show(True)
        self.Layout()

        
    def goBack(self, e):
        for panel in self.panels:
            panel.Hide()
        self.buildOutputPanel.Show(True)
        self.Layout()
        

    
        
app = wx.App()
window = BuildProfileAppFrame(None, title=g_appName, size=(800, 600))
#window.createBuildOutputPanel()
#window.createBuildProfilePanel()

window.Show(True)
app.MainLoop()
