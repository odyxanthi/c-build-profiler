""" A script to remove unused headers from .cpp files. Works by invoking compiler from the command line.
    In order for compilation to succeed, you need to set up the include directories correctly (use the
    g_includeDirs for this purpose)
    
    NOTE: In order to work, this script has to be run from the developer command prompt, 
          for which there should be a shortcut in 
          C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\Tools\Shortcuts
          (assumming VS2013 is used)
"""

import sys
import re
import shutil
import os
import subprocess

g_modificationsPerFile = dict()

# Modify the include directories according to the case.
base = 'D:/CODE/GIT_Legion_mine/'
g_includeDirs = [ base
                , base + '../Legion Third Party/Include'
				, base + 'LegionSimulator' ]
	
#cl /c /I .\ /I ..\  /I "..\..\Legion Third Party\Include" Entity.cpp



def getCppHeaders(filename):
    try:
        excluded = ['stdafx.h', 'DebugNew.hpp', 'TestRunnerHook.h','gtest.h']
        excluded = map(lambda s: s.lower(), excluded)

        f = open(filename,'r')
        headers = list()
        lines = f.read().splitlines()
        for line in lines:
            pos = line.lstrip().find('#include')
            if pos == 0:		#line begins with '#include'
                if any( line.lower().find(ex)>=0 for ex in excluded ) :
                    continue
                else:
                    headers.append(line)
        f.close()
        return headers
    except BaseException as e:
        print 'Failed to extrach headers from ', filename, '\n -> ', str(e) 
        return []
		
		
def commentMatchingLineInFile(filename, searchStr):
	f = open(filename,'r')
	lines = f.read().splitlines()
	f.close()
	
	f = open(filename,'w')
	for line in lines:
		if line.find(searchStr) >= 0:
			line = '// ' + line + ' // -> line automatically commented out'
		f.write(line+'\n')
	f.close()

	
def commentUnneededHeadersInFile(filename, compilesSuccessfullyPredicate ):
    name, ext = os.path.splitext(filename)
    tmpFile = name+'__tmp__'+ext
    try:
        headers = getCppHeaders(filename)
        headers.reverse()

        print 'processing file: ', filename
        for h in headers:
            shutil.copyfile(filename, tmpFile)
            commentMatchingLineInFile(tmpFile, h)
            print '\tcommenting line: ', h
            if compilesSuccessfullyPredicate(tmpFile):
                shutil.copyfile(tmpFile, filename)	# tmpFile compiles successfully, overwrite original file
            else:
                pass
            print '\n'
        return True
    except BaseException as e:
        print 'Failed to process file ', filename, '\n -> ', str(e)
        return False
    finally:
        if os.path.exists(tmpFile):
            os.remove(tmpFile)

		
def fileCompilesSuccessfully_MSVC12(filename, includeDirectories):
	args = ['cl.exe', '/c']
	for dir in includeDirectories:
		args.append('/I')
		args.append('"' + dir + '"')
	args.append(filename)
    
	try:
		subprocess.check_output(args) # shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		print '\t\t -> Compiled successfully'
		return True
	except subprocess.CalledProcessError as e:
		print '\t\t -> Compilation completed with error code: ', e.returncode
		return False

	
def fileCompilesSuccessfully_Mock(filename):
	n = g_modificationsPerFile.get(filename, 0)
	g_modificationsPerFile[filename] = n+1
	if n % 3 == 0:
		return False
	return True
	
	
def main():
    if len(sys.argv) < 2:
        print 'usage: removeUnneededHeaders.py <folder>'
        print '  -> searches recursively in <folder> for cpp files and removes unused headers'
        return

	# commentUnneededHeadersInFile(filename, lambda filename: fileCompilesSuccessfully_Mock(filename))
    folder = sys.argv[1]
    matchingFiles = list()
    for (root, dirs, files) in os.walk(folder):
        for file in files:
            _, ext = os.path.splitext(file)
            if ext == '.cpp':
                #matchingFiles.append(file)
                fullPath = os.path.join(root, file)
                matchingFiles.append(os.path.normpath(fullPath))
    
    def fileCompiles(filename):
        return fileCompilesSuccessfully_MSVC12(filename, g_includeDirs)	
    
    for file in matchingFiles:
        try:
            print file 
            commentUnneededHeadersInFile(file, fileCompiles )
        except BaseException as ex:
            print 'error occured while processing file ', file, ' --> ', str(ex) 


if __name__ == '__main__':
	main()