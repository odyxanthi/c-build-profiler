import sys
import re
import shutil
import os


g_topLevelHeaders = [
      '(.*sigslot.*)'
    , '(.*glm.*detail.*)'
    #   '(.*boost.*)'
    # , '(.*geometrix.*)'
    # , '(.*LegionUtility.*)'
    # , '(.*GenericObject.*)'
    # , '(.*atlmfc.*)'
    # , '(.*Microsoft.*)'
    # , '(.*gmock.*)'
]


def isLeafHeader(filename, leafHeaderPatterns):
    for pattern in leafHeaderPatterns:
        if len(re.findall(pattern, filename)) > 0:
            return True
    return False


def hideChildHeaders(buildOutputLines, leafHeaderPatterns):
    inclusionPattern = '.*Note: including file:(\s*)(.*)'                     #matches all included files   
    inclusionStack = list()
    modifiedBuildOutput = list()

    for line in buildOutputLines:
        line = line.rstrip()
        matches = re.findall(inclusionPattern, line)
        if len(matches) > 0:    # if this build output line indeed refers to a file being included
            numSpaces = len(matches[0][0])
            filename = matches[0][1]
            inclusionStack = inclusionStack[0: numSpaces - 1]

            # find wether the parent header is a leaf or not
            parentIsLeaf = False
            if len(inclusionStack) > 0:
                parentHeaderInfo = inclusionStack[-1]
                parentIsLeaf = parentHeaderInfo[1]

            # if parent header is leaf, then hide this line from output
            if parentIsLeaf:
                modifiedBuildOutput.append('---')
            else:
                modifiedBuildOutput.append(line)

            # add this file to the inclusion stack
            # see if it's a leaf file: it is a leaf if its parent is leaf or if it matches the patterns
            currentFileIsLeaf = False
            if parentIsLeaf or isLeafHeader(filename, leafHeaderPatterns):
                currentFileIsLeaf = True
            inclusionStack.append((filename, currentFileIsLeaf))
        else:
            # this build output line is irrelevant
            modifiedBuildOutput.append(line)

    return modifiedBuildOutput


def main():
    if len(sys.argv) < 2:
        print 'usage: extractMostIncludedHeaders.py <file>'
        return

    filename = sys.argv[1]
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

    lines2 = hideChildHeaders(lines, g_topLevelHeaders)
    for line in lines2:
        print line

if __name__ == '__main__':
	main()